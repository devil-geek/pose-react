import React from "react";
import "@tensorflow/tfjs";
import * as posenet from "@tensorflow-models/posenet";
import { drawSkeleton, drawKeypoints } from "./utils";
import "./App.css";

import demo from "./demo.mp4";

const videoWidth = 700;
const videoHeight = 400;
let flipPoseHorizontal = true;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  videoRef = React.createRef();
  canvasRef = React.createRef();

  setupCamera = async (type) => {
    const video = this.videoRef.current;
    video.width = videoWidth;
    video.height = videoHeight;
    if (type === "camera") {
      if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        throw new Error(
          "Browser API navigator.mediaDevices.getUserMedia not available"
        );
      }

      const stream = await navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          facingMode: "user",
          width: videoWidth,
          height: videoHeight,
        },
      });
      video.srcObject = stream;
    } else {
      video.src = demo;
    }
    return new Promise((resolve) => {
      video.onloadedmetadata = () => {
        resolve(video);
      };
    });
  };

  loadVideo = async (type) => {
    const video = await this.setupCamera(type);
    video.play();
    return video;
  };

  /**
   * Feeds an image to posenet to estimate poses - this is where the magic
   * happens. This function loops with a requestAnimationFrame method.
   */
  detectPoseInRealTime = (video, net) => {
    const canvas = this.canvasRef.current;
    const ctx = canvas.getContext("2d");

    // since images are being fed from a webcam, we want to feed in the
    // original image and then just flip the keypoints' x coordinates. If instead
    // we flip the image, then correcting left-right keypoint pairs requires a
    // permutation on all the keypoints.

    canvas.width = videoWidth;
    canvas.height = videoHeight;

    async function poseDetectionFrame() {
      let poses = [];
      let minPoseConfidence;
      let minPartConfidence;

      /*  
      Single pose

      const pose = await net.estimatePoses(video, {
        flipHorizontal: flipPoseHorizontal,
        decodingMethod: "single-person",
      });
      poses = poses.concat(pose);
      minPoseConfidence = +0.1;
      minPartConfidence = +0.5; 
      
      */

      let all_poses = await net.estimatePoses(video, {
        flipHorizontal: flipPoseHorizontal,
        decodingMethod: "multi-person",
        maxDetections: 5,
        scoreThreshold: 0.8,
        nmsRadius: 40,
      });

      poses = poses.concat(all_poses);
      minPoseConfidence = +0.15;
      minPartConfidence = +0.1;

      ctx.clearRect(0, 0, videoWidth, videoHeight);

      if (flipPoseHorizontal) {
        ctx.save();
        ctx.scale(-1, 1);
        ctx.translate(-videoWidth, 0);
        ctx.drawImage(video, 0, 0, videoWidth, videoHeight);
        ctx.restore();
      }

      // For each pose (i.e. person) detected in an image, loop through the poses
      // and draw the resulting skeleton and keypoints if over certain confidence
      // scores
      poses.forEach(({ score, keypoints }) => {
        if (score >= minPoseConfidence) {
          drawKeypoints(keypoints, minPartConfidence, ctx);
          drawSkeleton(keypoints, minPartConfidence, ctx);
        }
      });

      requestAnimationFrame(poseDetectionFrame);
    }

    poseDetectionFrame();
  };

  init = async (type) => {
    const { loading } = this.state;
    this.setState({
      loading: true,
    });
    const net = await posenet.load({
      /*architecture: "ResNet50",
      outputStride: 32,
      inputResolution: 250,
      multiplier: 1.0,
      quantBytes: 2, */
      architecture: "MobileNetV1",
      outputStride: 16,
      inputResolution: 500,
      multiplier: 0.75,
      quantBytes: 2,
    });
    let video;

    flipPoseHorizontal = type === "camera" ? true : false;

    try {
      video = await this.loadVideo(type);
      this.setState({
        loading: false,
      });
    } catch (e) {
      console.error("this browser does not support video capture");
      throw e;
    }
    if (!loading) {
      this.detectPoseInRealTime(video, net);
    }
  };

  render() {
    const { loading } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <button className="App-link" onClick={() => this.init("camera")}>
            From web cam
          </button>
          <button className="App-link" onClick={() => this.init("video")}>
            From demo video
          </button>
          <div>
            {loading && <p>Loading model...</p>}
            <video className="size hidden" ref={this.videoRef} />
            <canvas className="size" ref={this.canvasRef} />
          </div>
        </header>
      </div>
    );
  }
}

export default App;
